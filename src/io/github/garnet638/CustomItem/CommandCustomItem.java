package io.github.garnet638.CustomItem;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.sun.xml.internal.ws.api.server.DocumentAddressResolver;

@SuppressWarnings({ "unused", "deprecation" })
public class CommandCustomItem implements CommandExecutor{
	
	//Colors
	static String white = ChatColor.WHITE+"";
	static String gray = ChatColor.GRAY+"";
	static String dgray = ChatColor.DARK_GRAY+"";
	static String black = ChatColor.BLACK+"";
	static String gold = ChatColor.GOLD+"";
	static String green = ChatColor.GREEN+"";
	static String red = ChatColor.RED+"";
	static String dblue = ChatColor.DARK_BLUE+"";
	//Formatting
	static String bold = ChatColor.BOLD+"";
	static String strikethrough = ChatColor.STRIKETHROUGH+"";
	static String italic = ChatColor.ITALIC+"";
	//Other
	static ArrayList<String> lore = new ArrayList<String>();
	static ItemStack filler = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.BLACK.getData());
	static{	
		ItemMeta fillerM = filler.getItemMeta();
		fillerM.setDisplayName(gold+" ");
		filler.setItemMeta(fillerM);
	}
	static ItemStack back = new ItemStack(Material.WOOL, 1, DyeColor.RED.getData());
	static{	
		ItemMeta backM = back.getItemMeta();
		backM.setDisplayName(gold+"Go back");
		back.setItemMeta(backM);
	}
	static ItemStack lvl1 = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.LIME.getData());
	static{	
		ItemMeta lvlM = back.getItemMeta();
		lvlM.setDisplayName(green+"Level 1");
		lvl1.setItemMeta(lvlM);
	}
	static ItemStack lvl2 = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.LIME.getData());
	static{	
		ItemMeta lvlM = back.getItemMeta();
		lvlM.setDisplayName(green+"Level 2");
		lvl2.setItemMeta(lvlM);
	}
	static ItemStack lvl3 = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.LIME.getData());
	static{	
		ItemMeta lvlM = back.getItemMeta();
		lvlM.setDisplayName(green+"Level 3");
		lvl3.setItemMeta(lvlM);
	}
	static ItemStack lvl4 = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.LIME.getData());
	static{	
		ItemMeta lvlM = back.getItemMeta();
		lvlM.setDisplayName(green+"Level 4");
		lvl4.setItemMeta(lvlM);
	}
	static ItemStack lvl5 = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.LIME.getData());
	static{	
		ItemMeta lvlM = back.getItemMeta();
		lvlM.setDisplayName(green+"Level 5");
		lvl5.setItemMeta(lvlM);
	}	
	static ItemStack rem = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.RED.getData());
	static {	
		ItemMeta lvlM = back.getItemMeta();
		lvlM.setDisplayName(green+"Remove Enchantment");
		rem.setItemMeta(lvlM);
	}
	
	
	///
	/// GUIs and shit
	///
	static int build = 100;
	static int prvBuild = 0;
	static String version = "1.0."+(build-prvBuild);
	
	//Make GUIs
	public static Inventory mainmenu = Bukkit.createInventory(null, 9, dblue+bold+"Custom Items");
	static {
		        
		//Make an item
	    ItemStack makeItem = new ItemStack(Material.APPLE);
	    ItemMeta makeItemMeta = makeItem.getItemMeta();
	    makeItemMeta.setDisplayName(gold+"Make an item");
	    makeItem.setItemMeta(makeItemMeta);
		
        //Make a spawner
        ItemStack makeSpawner = new ItemStack(Material.MOB_SPAWNER);
        ItemMeta makeSpawnerMeta = makeSpawner.getItemMeta();
        makeSpawnerMeta.setDisplayName(red+italic+"Make a spawner");
        lore.add(red+italic+"This feature is currently in development");
        makeSpawnerMeta.setLore(lore);
        makeSpawner.setItemMeta(makeSpawnerMeta);
        
        //Make a Firework
        ItemStack makeFirework = new ItemStack(Material.FIREWORK);
        ItemMeta makeFireworkMeta = makeFirework.getItemMeta();
        makeFireworkMeta.setDisplayName(red+italic+"Make a firework");
        lore.clear();
        lore.add(red+italic+"This feature is currently in development");
        makeFireworkMeta.setLore(lore);
        makeFirework.setItemMeta(makeFireworkMeta);
        
        //Help
        ItemStack help = new ItemStack(Material.BOOK);
        ItemMeta helpMeta = help.getItemMeta();
        helpMeta.setDisplayName(gold+"Help");
        lore.clear();
        lore.add(gold+"Plugin made by: Garnet638");
        lore.add(white+"Click the apple to edit an item");
//        lore.add(white+"Click the spawner to edit a spawner");
        helpMeta.setLore(lore);
        help.setItemMeta(helpMeta);
        
        //Set items
        mainmenu.setItem(0, filler);
        mainmenu.setItem(1, filler);
        mainmenu.setItem(2, filler);
        mainmenu.setItem(3, makeItem);
        mainmenu.setItem(4, makeFirework);
        mainmenu.setItem(5, makeSpawner);
        mainmenu.setItem(6, filler);
        mainmenu.setItem(7, filler);
        mainmenu.setItem(8, help);
        
	}

	public static Inventory edititem = Bukkit.createInventory(null, 9, green+"Make an item");
	static {
        
		//Rename
	    ItemStack rename = new ItemStack(Material.NAME_TAG);
	    ItemMeta renameM = rename.getItemMeta();
	    renameM.setDisplayName(gold+"Rename");
	    rename.setItemMeta(renameM);
		//Lore
	    ItemStack lore = new ItemStack(Material.BOOK);
	    ItemMeta loreM = rename.getItemMeta();
	    loreM.setDisplayName(gold+"Lore");
	    lore.setItemMeta(loreM);
	    //Enchant
	    ItemStack ench = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta enchM = rename.getItemMeta();
	    enchM.setDisplayName(gold+"Enchant");
	    ench.setItemMeta(enchM);
	    //Attribute
	    ItemStack att = new ItemStack(Material.DIAMOND_CHESTPLATE);
	    ItemMeta attM = rename.getItemMeta();
	    attM.setDisplayName(gold+"Attributes");
	    att.setItemMeta(attM);
	    //Custom
	    ItemStack custom = new ItemStack(Material.TNT);
	    ItemMeta customM = rename.getItemMeta();
	    customM.setDisplayName(gold+"Misc Properties");
	    custom.setItemMeta(customM);
        
        
        //Set items
	    edititem.setItem(0, back);
	    edititem.setItem(1, filler);
	    edititem.setItem(2, rename);
	    edititem.setItem(3, lore);
	    edititem.setItem(4, filler);
        edititem.setItem(5, ench);
        edititem.setItem(6, custom);
        edititem.setItem(7, filler);
        edititem.setItem(8, back);
        
	}
	
	public static Inventory renameGUI = Bukkit.createInventory(null, 9, green+"Rename");
	static {
		//Rename
	    ItemStack rename = new ItemStack(Material.EMERALD_BLOCK);
	    ItemMeta renameM = rename.getItemMeta();
	    renameM.setDisplayName(gold+"Rename");
	    rename.setItemMeta(renameM);
		//Lore
	    ItemStack removeName = new ItemStack(Material.REDSTONE_BLOCK);
	    ItemMeta removeNameM = rename.getItemMeta();
	    removeNameM.setDisplayName(gold+"Remove name");
	    removeName.setItemMeta(removeNameM);
      
        
        //Set items
	    renameGUI.setItem(0, back);
	    renameGUI.setItem(1, filler);
	    renameGUI.setItem(2, filler);
	    renameGUI.setItem(3, rename);
	    renameGUI.setItem(4, filler);
	    renameGUI.setItem(5, removeName);
        renameGUI.setItem(6, filler);
        renameGUI.setItem(7, filler);
        renameGUI.setItem(8, back);
	}
	
	public static Inventory loreGUI = Bukkit.createInventory(null, 9, green+"Lore");
	static {
		//Rename
	    ItemStack lore = new ItemStack(Material.EMERALD_BLOCK);
	    ItemMeta loreM = lore.getItemMeta();
	    loreM.setDisplayName(gold+"Set lore");
	    lore.setItemMeta(loreM);
		//Lore
	    ItemStack removeLore = new ItemStack(Material.REDSTONE_BLOCK);
	    ItemMeta removeLoreM = lore.getItemMeta();
	    removeLoreM.setDisplayName(gold+"Remove lore");
	    removeLore.setItemMeta(removeLoreM);
      
        
        //Set items
	    loreGUI.setItem(0, back);
	    loreGUI.setItem(1, filler);
	    loreGUI.setItem(2, filler);
	    loreGUI.setItem(3, lore);
	    loreGUI.setItem(4, filler);
	    loreGUI.setItem(5, removeLore);
        loreGUI.setItem(6, filler);
        loreGUI.setItem(7, filler);
        loreGUI.setItem(8, back);
	}

	public static Inventory enchGUI = Bukkit.createInventory(null, 9, green+"Enchant");
	static {
		//Global
	    ItemStack global = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta globalM = global.getItemMeta();
	    globalM.setDisplayName(gold+"Global Enchantments");
	    global.setItemMeta(globalM);
		//Sword
	    ItemStack sword = new ItemStack(Material.DIAMOND_SWORD);
	    ItemMeta swordM = sword.getItemMeta();
	    swordM.setDisplayName(gold+"Sword Enchantments");
	    sword.setItemMeta(swordM);
	    //bow
	    ItemStack bow = new ItemStack(Material.BOW);
	    ItemMeta bowM = bow.getItemMeta();
	    bowM.setDisplayName(gold+"Bow Enchantments");
	    bow.setItemMeta(bowM);
	    //armor
	    ItemStack armor = new ItemStack(Material.DIAMOND_CHESTPLATE);
	    ItemMeta armorM = armor.getItemMeta();
	    armorM.setDisplayName(gold+"Armor Enchantments");
	    armor.setItemMeta(armorM);
	    //tool
	    ItemStack tool = new ItemStack(Material.DIAMOND_PICKAXE);
	    ItemMeta toolM = tool.getItemMeta();
	    toolM.setDisplayName(gold+"Tool Enchantments");
	    tool.setItemMeta(toolM);
        
        
        //Set items
	    enchGUI.setItem(0, back);
	    enchGUI.setItem(1, filler);
	    enchGUI.setItem(2, global);
	    enchGUI.setItem(3, sword);
	    enchGUI.setItem(4, bow);
	    enchGUI.setItem(5, armor);
        enchGUI.setItem(6, tool);
        enchGUI.setItem(7, filler);
        enchGUI.setItem(8, back);
	}

	public static Inventory enchGUIglobal = Bukkit.createInventory(null, 18, green+"Global Enchantments");
	static {
		//unbreaking
	    ItemStack unbreaking = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta unbreakingM = unbreaking.getItemMeta();
	    unbreakingM.setDisplayName(gold+"Unbreaking");
	    unbreaking.setItemMeta(unbreakingM);
	    //mending
	    ItemStack mending = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta mendingM = mending.getItemMeta();
	    mendingM.setDisplayName(gold+"Mending");
	    mending.setItemMeta(mendingM);
        
        //Set items
	    enchGUIglobal.setItem(0, back);
	    enchGUIglobal.setItem(1, rem);
	    enchGUIglobal.setItem(2, lvl1);
	    enchGUIglobal.setItem(3, lvl2);
	    enchGUIglobal.setItem(4, lvl3);
	    enchGUIglobal.setItem(5, lvl4);
        enchGUIglobal.setItem(6, lvl5);
        enchGUIglobal.setItem(7, rem);
        enchGUIglobal.setItem(8, back);
        enchGUIglobal.setItem(9, back);
	    enchGUIglobal.setItem(10, filler);
	    enchGUIglobal.setItem(11, filler);
	    enchGUIglobal.setItem(12, unbreaking);
	    enchGUIglobal.setItem(13, filler);
	    enchGUIglobal.setItem(14, mending);
        enchGUIglobal.setItem(15, filler);
        enchGUIglobal.setItem(16, filler);
        enchGUIglobal.setItem(17, back);
	}

	public static Inventory enchGUIsword = Bukkit.createInventory(null, 18, green+"Sword Enchantments");
	static {
		//sharp
	    ItemStack sharp = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta sharpM = sharp.getItemMeta();
	    sharpM.setDisplayName(gold+"Sharpness");
	    sharp.setItemMeta(sharpM);
	    //smite
	    ItemStack smite = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta smiteM = smite.getItemMeta();
	    smiteM.setDisplayName(gold+"Smite");
	    smite.setItemMeta(smiteM);
	    //bane
	    ItemStack bane = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta baneM = bane.getItemMeta();
	    baneM.setDisplayName(gold+"Bane of Arthropods");
	    bane.setItemMeta(baneM);
	    //knockback
	    ItemStack knockback = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta knockbackM = knockback.getItemMeta();
	    knockbackM.setDisplayName(gold+"Knockback");
	    knockback.setItemMeta(knockbackM);
	    //faspect
	    ItemStack faspect = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta faspectM = faspect.getItemMeta();
	    faspectM.setDisplayName(gold+"Fire Aspect");
	    faspect.setItemMeta(faspectM);
	    //looting
	    ItemStack looting = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta lootingM = looting.getItemMeta();
	    lootingM.setDisplayName(gold+"Looting");
	    looting.setItemMeta(lootingM);
        
        //Set items
	    enchGUIsword.setItem(0, back);
	    enchGUIsword.setItem(1, rem);
	    enchGUIsword.setItem(2, lvl1);
	    enchGUIsword.setItem(3, lvl2);
	    enchGUIsword.setItem(4, lvl3);
	    enchGUIsword.setItem(5, lvl4);
        enchGUIsword.setItem(6, lvl5);
        enchGUIsword.setItem(7, rem);
        enchGUIsword.setItem(8, back);
        enchGUIsword.setItem(9, back);
	    enchGUIsword.setItem(10, sharp);
	    enchGUIsword.setItem(11, smite);
	    enchGUIsword.setItem(12, bane);
	    enchGUIsword.setItem(13, filler);
	    enchGUIsword.setItem(14, knockback);
        enchGUIsword.setItem(15, faspect);
        enchGUIsword.setItem(16, looting);
        enchGUIsword.setItem(17, back);
	}

	public static Inventory enchGUIbow = Bukkit.createInventory(null, 18, green+"Bow Enchantments");
	static {
		//power
	    ItemStack power = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta powerM = power.getItemMeta();
	    powerM.setDisplayName(gold+"Power");
	    power.setItemMeta(powerM);
	    //punch
	    ItemStack punch = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta punchM = punch.getItemMeta();
	    punchM.setDisplayName(gold+"Punch");
	    punch.setItemMeta(punchM);
	    //flame
	    ItemStack flame = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta flameM = flame.getItemMeta();
	    flameM.setDisplayName(gold+"Flame");
	    flame.setItemMeta(flameM);
	    //infinity
	    ItemStack infinity = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta infinityM = infinity.getItemMeta();
	    infinityM.setDisplayName(gold+"Infinity");
	    infinity.setItemMeta(infinityM);

        
        //Set items
	    enchGUIbow.setItem(0, back);
	    enchGUIbow.setItem(1, rem);
	    enchGUIbow.setItem(2, lvl1);
	    enchGUIbow.setItem(3, lvl2);
	    enchGUIbow.setItem(4, lvl3);
	    enchGUIbow.setItem(5, lvl4);
        enchGUIbow.setItem(6, lvl5);
        enchGUIbow.setItem(7, rem);
        enchGUIbow.setItem(8, back);
        enchGUIbow.setItem(9, back);
	    enchGUIbow.setItem(10, filler);
	    enchGUIbow.setItem(11, power);
	    enchGUIbow.setItem(12, punch);
	    enchGUIbow.setItem(13, filler);
	    enchGUIbow.setItem(14, flame);
        enchGUIbow.setItem(15, infinity);
        enchGUIbow.setItem(16, filler);
        enchGUIbow.setItem(17, back);
	}

	public static Inventory enchGUIarmor = Bukkit.createInventory(null, 27, green+"Armor Enchantments");
	static {
		//prot
	    ItemStack prot = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta protM = prot.getItemMeta();
	    protM.setDisplayName(gold+"Protection");
	    prot.setItemMeta(protM);
	    //fprot
	    ItemStack fprot = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta fprotM = fprot.getItemMeta();
	    fprotM.setDisplayName(gold+"Fire Protection");
	    fprot.setItemMeta(fprotM);
	    //bprot
	    ItemStack bprot = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta bprotM = bprot.getItemMeta();
	    bprotM.setDisplayName(gold+"Blast Protection");
	    bprot.setItemMeta(bprotM);
	    //pprot
	    ItemStack pprot = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta pprotM = pprot.getItemMeta();
	    pprotM.setDisplayName(gold+"Projectile Protection");
	    pprot.setItemMeta(pprotM);
	    //thorns
	    ItemStack thorns = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta thornsM = thorns.getItemMeta();
	    thornsM.setDisplayName(gold+"Thorns");
	    thorns.setItemMeta(thornsM);
	    //resp
	    ItemStack resp = new ItemStack(Material.DIAMOND_HELMET);
	    ItemMeta respM = resp.getItemMeta();
	    respM.setDisplayName(gold+"Respiration");
	    resp.setItemMeta(respM);
	    //aa
	    ItemStack aa = new ItemStack(Material.DIAMOND_HELMET);
	    ItemMeta aaM = aa.getItemMeta();
	    aaM.setDisplayName(gold+"Aqua Affinity");
	    aa.setItemMeta(aaM);
	    //ffalling
	    ItemStack ffalling = new ItemStack(Material.DIAMOND_BOOTS);
	    ItemMeta ffallingM = ffalling.getItemMeta();
	    ffallingM.setDisplayName(gold+"Feather Falling");
	    ffalling.setItemMeta(ffallingM);
	    //dstrider
	    ItemStack dstrider = new ItemStack(Material.DIAMOND_BOOTS);
	    ItemMeta dstriderM = dstrider.getItemMeta();
	    dstriderM.setDisplayName(gold+"Depth Strider");
	    dstrider.setItemMeta(dstriderM);
	    //fwalker
	    ItemStack fwalker = new ItemStack(Material.DIAMOND_BOOTS);
	    ItemMeta fwalkerM = fwalker.getItemMeta();
	    fwalkerM.setDisplayName(gold+"Frost Walker");
	    fwalker.setItemMeta(fwalkerM);

        
        //Set items
	    enchGUIarmor.setItem(0, back);
	    enchGUIarmor.setItem(1, rem);
	    enchGUIarmor.setItem(2, lvl1);
	    enchGUIarmor.setItem(3, lvl2);
	    enchGUIarmor.setItem(4, lvl3);
	    enchGUIarmor.setItem(5, lvl4);
        enchGUIarmor.setItem(6, lvl5);
        enchGUIarmor.setItem(7, rem);
        enchGUIarmor.setItem(8, back);
        enchGUIarmor.setItem(9, back);
	    enchGUIarmor.setItem(10, filler);
	    enchGUIarmor.setItem(11, prot);
	    enchGUIarmor.setItem(12, fprot);
	    enchGUIarmor.setItem(13, bprot);
	    enchGUIarmor.setItem(14, pprot);
        enchGUIarmor.setItem(15, thorns);
        enchGUIarmor.setItem(16, filler);
        enchGUIarmor.setItem(17, back);
        enchGUIarmor.setItem(18, back);
	    enchGUIarmor.setItem(19, filler);
	    enchGUIarmor.setItem(20, resp);
	    enchGUIarmor.setItem(21, aa);
	    enchGUIarmor.setItem(22, ffalling);
	    enchGUIarmor.setItem(23, dstrider);
        enchGUIarmor.setItem(24, fwalker);
        enchGUIarmor.setItem(25, filler);
        enchGUIarmor.setItem(26, back);
	}
	
	public static Inventory enchGUItools = Bukkit.createInventory(null, 18, green+"Tool Enchantments");
	static {
		//eff
	    ItemStack eff = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta effM = eff.getItemMeta();
	    effM.setDisplayName(gold+"Efficiency");
	    eff.setItemMeta(effM);
	    //silktouch
	    ItemStack silktouch = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta silktouchM = silktouch.getItemMeta();
	    silktouchM.setDisplayName(gold+"Silk Touch");
	    silktouch.setItemMeta(silktouchM);
	    //fortune
	    ItemStack fortune = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta fortuneM = fortune.getItemMeta();
	    fortuneM.setDisplayName(gold+"Fortune");
	    fortune.setItemMeta(fortuneM);
	    //luckofthesea
	    ItemStack luckofthesea = new ItemStack(Material.FISHING_ROD);
	    ItemMeta luckoftheseaM = luckofthesea.getItemMeta();
	    luckoftheseaM.setDisplayName(gold+"Luck of the Sea");
	    luckofthesea.setItemMeta(luckoftheseaM);
	    //lure
	    ItemStack lure = new ItemStack(Material.FISHING_ROD);
	    ItemMeta lureM = lure.getItemMeta();
	    lureM.setDisplayName(gold+"Lure");
	    lure.setItemMeta(lureM);
        
        //Set items
	    enchGUItools.setItem(0, back);
	    enchGUItools.setItem(1, rem);
	    enchGUItools.setItem(2, lvl1);
	    enchGUItools.setItem(3, lvl2);
	    enchGUItools.setItem(4, lvl3);
	    enchGUItools.setItem(5, lvl4);
        enchGUItools.setItem(6, lvl5);
        enchGUItools.setItem(7, rem);
        enchGUItools.setItem(8, back);
        enchGUItools.setItem(9, back);
	    enchGUItools.setItem(10, filler);
	    enchGUItools.setItem(11, eff);
	    enchGUItools.setItem(12, silktouch);
	    enchGUItools.setItem(13, fortune);
	    enchGUItools.setItem(14, luckofthesea);
        enchGUItools.setItem(15, lure);
        enchGUItools.setItem(16, filler);
        enchGUItools.setItem(17, back);
	}
	
	public static Inventory attribGUI = Bukkit.createInventory(null, 18, green+"Attribute Editor");
	static {
		//maxHP
	    ItemStack maxHP = new ItemStack(Material.GOLDEN_APPLE);
	    ItemMeta maxHPM = maxHP.getItemMeta();
	    maxHPM.setDisplayName(gold+"Max Health");
	    maxHP.setItemMeta(maxHPM);
	    //follow
	    ItemStack follow = new ItemStack(Material.LEASH);
	    ItemMeta followM = follow.getItemMeta();
	    followM.setDisplayName(gold+" Range");
	    follow.setItemMeta(followM);
	    //knockbackRes
	    ItemStack knockbackRes = new ItemStack(Material.SHIELD);
	    ItemMeta knockbackResM = knockbackRes.getItemMeta();
	    knockbackResM.setDisplayName(gold+"Knockback Resistance");
	    knockbackRes.setItemMeta(knockbackResM);
	    //moveSpeed
	    ItemStack moveSpeed = new ItemStack(Material.SUGAR);
	    ItemMeta moveSpeedM = moveSpeed.getItemMeta();
	    moveSpeedM.setDisplayName(gold+"Movement Speed");
	    moveSpeed.setItemMeta(moveSpeedM);
	    //atkDMG
	    ItemStack atkDMG = new ItemStack(Material.DIAMOND_SWORD);
	    ItemMeta atkDMGM = atkDMG.getItemMeta();
	    atkDMGM.setDisplayName(gold+"Attack Damage");
	    atkDMG.setItemMeta(atkDMGM);
	    //armor
	    ItemStack armor = new ItemStack(Material.DIAMOND_CHESTPLATE);
	    ItemMeta armorM = armor.getItemMeta();
	    armorM.setDisplayName(gold+" Armor Points");
	    armor.setItemMeta(armorM);
	    //armorToughness
	    ItemStack armorToughness = new ItemStack(Material.OBSIDIAN);
	    ItemMeta armorToughnessM = armorToughness.getItemMeta();
	    armorToughnessM.setDisplayName(gold+"Armor Toughness");
	    armorToughness.setItemMeta(armorToughnessM);
	    //atkSpeed
	    ItemStack atkSpeed = new ItemStack(Material.IRON_SWORD);
	    ItemMeta atkSpeedM = atkSpeed.getItemMeta();
	    atkSpeedM.setDisplayName(gold+"Attack Speed");
	    atkSpeed.setItemMeta(atkSpeedM);
	    //luck
	    ItemStack luck = new ItemStack(Material.ENCHANTED_BOOK);
	    ItemMeta luckM = luck.getItemMeta();
	    luckM.setDisplayName(gold+"Attack Damage");
	    luck.setItemMeta(luckM);
        
        //Set items
	    attribGUI.setItem(0, back);
	    attribGUI.setItem(1, filler);
	    attribGUI.setItem(2, maxHP);
	    attribGUI.setItem(3, follow);
	    attribGUI.setItem(4, knockbackRes);
	    attribGUI.setItem(5, moveSpeed);
        attribGUI.setItem(6, atkDMG);
        attribGUI.setItem(7, filler);
        attribGUI.setItem(8, back);
        attribGUI.setItem(9, back);
	    attribGUI.setItem(10, filler);
	    attribGUI.setItem(11, armor);
	    attribGUI.setItem(12, armorToughness);
	    attribGUI.setItem(13, filler);
	    attribGUI.setItem(14, atkSpeed);
        attribGUI.setItem(15, luck);
        attribGUI.setItem(16, filler);
        attribGUI.setItem(17, back);
	}

	public static Inventory customGUI = Bukkit.createInventory(null, 9, green+"Misc Properties");
	static {
	    //unbreakable
	    ItemStack unbreakable = new ItemStack(Material.BEDROCK);
	    ItemMeta unbreakableM = unbreakable.getItemMeta();
	    unbreakableM.setDisplayName(gold+"Unbreakable");
	    unbreakable.setItemMeta(unbreakableM);
	    //wipe enchantments
	    ItemStack wipeEnch = new ItemStack(Material.BOOK);
	    ItemMeta wipeEnchM = wipeEnch.getItemMeta();
	    wipeEnchM.setDisplayName(gold+"Wipe all Enchantments");
	    wipeEnch.setItemMeta(wipeEnchM);
	    //wipe item meta
	    ItemStack wipeData = new ItemStack(Material.DIRT);
	    ItemMeta wipeDataM = wipeData.getItemMeta();
	    wipeDataM.setDisplayName(gold+"Wipe the item's data");
	    wipeData.setItemMeta(wipeDataM);
        
        //Set items
	    customGUI.setItem(0, back);
	    customGUI.setItem(1, filler);
	    customGUI.setItem(2, filler);
	    customGUI.setItem(3, unbreakable);
	    customGUI.setItem(4, wipeEnch);
	    customGUI.setItem(5, wipeData);
        customGUI.setItem(6, filler);
        customGUI.setItem(7, filler);
        customGUI.setItem(8, back);

	}
	
	public static Inventory fireworkGUI = Bukkit.createInventory(null, 9, green+"Firework editor");
	static {
		//Rename
	    ItemStack fireworkcolor = new ItemStack(Material.INK_SACK, 1, (short)10);
	    ItemMeta fireworkcolorM = fireworkcolor.getItemMeta();
	    fireworkcolorM.setDisplayName(gold+"Change color");
	    fireworkcolor.setItemMeta(fireworkcolorM);
	    //Rename
	    ItemStack fireworkfadecolor = new ItemStack(Material.REDSTONE);
	    ItemMeta fireworkfadecolorM = fireworkfadecolor.getItemMeta();
	    fireworkfadecolorM.setDisplayName(gold+"Change fade color");
	    fireworkfadecolor.setItemMeta(fireworkfadecolorM);
		//Lore
	    ItemStack setEffect = new ItemStack(Material.DIAMOND);
	    ItemMeta setEffectM = setEffect.getItemMeta();
	    setEffectM.setDisplayName(gold+"Change effect");
	    setEffect.setItemMeta(setEffectM);
	    //Length
	    ItemStack setLength = new ItemStack(Material.SULPHUR);
	    ItemMeta setLengthM = setLength.getItemMeta();
	    setLengthM.setDisplayName(gold+"Set flight duration");
	    setLength.setItemMeta(setLengthM);
	  //Length
	    ItemStack setTrail = new ItemStack(Material.NETHER_STAR);
	    ItemMeta setTrailM = setTrail.getItemMeta();
	    setTrailM.setDisplayName(gold+"Toggle trail");
	    setTrail.setItemMeta(setTrailM);
      
        
        //Set items
	    fireworkGUI.setItem(0, back);
	    fireworkGUI.setItem(1, filler);
	    fireworkGUI.setItem(2, fireworkcolor);
	    fireworkGUI.setItem(3, fireworkfadecolor);
	    fireworkGUI.setItem(4, setEffect);
	    fireworkGUI.setItem(5, setLength);
	    fireworkGUI.setItem(6, setTrail);
        fireworkGUI.setItem(7, filler);
        fireworkGUI.setItem(8, back);
	}
	
	
public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		if (cmd.getName().equalsIgnoreCase("customitem") && sender instanceof Player){
									
			//Generic Vars
			Player player = (Player) sender;
			int length = args.length;
			
			//Help
			if (length == 0){
					
				player.sendMessage(dgray+bold+">"+strikethrough+"-----------------"+black+"["+gold+"Custom Items"+black+"]"+dgray+bold+strikethrough+"----------------"+dgray+bold+"<");
				player.sendMessage(gold+"Version "+version+" (Major.Minor.Build)");
				player.sendMessage(gold+"Build: "+build);
				player.sendMessage(gold+"/customitem menu"+gray+" Brings up main menu");
				player.sendMessage(gold+"/customitem makeitem"+gray+" Brings up the item menu");
			}
			
			
			//GUI
			if (length == 1 && args[0].equals("menu")){
				if (player.hasPermission("customitem.use")){
					player.openInventory(mainmenu);			
				}
				if (!player.hasPermission("customitem.use")){
					player.sendMessage(ChatColor.BLACK+"["+ChatColor.GOLD+"CustomItem"+ChatColor.BLACK+"] "+ChatColor.RED+"You do not have permission to use this command.");
				}
			}
			
			//GUI
			if (length == 1 && args[0].equals("makeitem")){
				if (player.hasPermission("customitem.use")){
					player.openInventory(edititem);			
				}
				if (!player.hasPermission("customitem.use")){
					player.sendMessage(ChatColor.BLACK+"["+ChatColor.GOLD+"CustomItem"+ChatColor.BLACK+"] "+ChatColor.RED+"You do not have permission to use this command.");
				}
			}
			
			return true;	
		}
		return false;
	}	

}