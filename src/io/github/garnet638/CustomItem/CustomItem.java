package io.github.garnet638.CustomItem;

import org.bukkit.plugin.java.JavaPlugin;

public class CustomItem extends JavaPlugin{
	@Override
	public void onEnable() {
		getLogger().info("Custom Items by Garnet638");
		getCommand("customitem").setExecutor(new CommandCustomItem());
		getCommand("citem").setExecutor(new CommandCustomItem());
		new GUIs(this);
	}
	
	@Override
	public void onDisable(){
		
	}
}
