package io.github.garnet638.CustomItem;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

@SuppressWarnings("unused")
public class GUIs extends CommandCustomItem implements Listener{
	
	//Register
	CustomItem plugin;
	
	public GUIs(CustomItem plugin){
        this.plugin = plugin;  
        plugin.getServer().getPluginManager().registerEvents(this, plugin);    
	}
	
	//Hashmap
	public HashMap<Player, Boolean> IsEditing = new HashMap<Player, Boolean>();
	public HashMap<Player, String> mode = new HashMap<Player, String>();
	static ArrayList<String> lore = new ArrayList<String>();
	public HashMap<Player, Integer> loreLine = new HashMap<Player, Integer>();
	public HashMap<Player, Integer> enchLvl = new HashMap<Player, Integer>();
	public HashMap<Player, Integer> tempInt = new HashMap<Player, Integer>();
	public String input = "";
	public String colorInput = "";
	public String noperms = ChatColor.RED+"Sorry, you do not have permission to use this";
	ItemStack ActualFirework = new ItemStack(Material.FIREWORK);
	
	ItemStack global = new ItemStack(Material.ENCHANTED_BOOK);
	{
	ItemMeta globalM = global.getItemMeta();
	globalM.setDisplayName(gold+"Global Enchantments");
	global.setItemMeta(globalM);
	}	
    ItemStack sword = new ItemStack(Material.DIAMOND_SWORD);
    {
    ItemMeta swordM = sword.getItemMeta();
    swordM.setDisplayName(gold+"Sword Enchantments");
    sword.setItemMeta(swordM);
    }
    ItemStack bow = new ItemStack(Material.BOW);
    {
    ItemMeta bowM = bow.getItemMeta();
    bowM.setDisplayName(gold+"Bow Enchantments");
    bow.setItemMeta(bowM);
	}
    ItemStack armor = new ItemStack(Material.DIAMOND_CHESTPLATE);
    {
    ItemMeta armorM = armor.getItemMeta();
    armorM.setDisplayName(gold+"Armor Enchantments");
    armor.setItemMeta(armorM);
	}
    ItemStack tool = new ItemStack(Material.DIAMOND_PICKAXE);
    {
    ItemMeta toolM = tool.getItemMeta();
    toolM.setDisplayName(gold+"Tool Enchantments");
    tool.setItemMeta(toolM);
	}
	
    @SuppressWarnings("deprecation")
	ItemStack rem = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.RED.getData());
	{	
		ItemMeta lvlM = back.getItemMeta();
		lvlM.setDisplayName(green+"Remove Enchantment");
		rem.setItemMeta(lvlM);
	}
	@SuppressWarnings("deprecation")
	ItemStack lvl1 = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.LIME.getData());
	{	
		ItemMeta lvlM = back.getItemMeta();
		lvlM.setDisplayName(green+"Level 1");
		lvl1.setItemMeta(lvlM);
	}
	@SuppressWarnings("deprecation")
	ItemStack lvl2 = new ItemStack(Material.STAINED_GLASS_PANE, 2, DyeColor.LIME.getData());
	{	
		ItemMeta lvl2M = back.getItemMeta();
		lvl2M.setDisplayName(green+"Level 2");
		lvl2.setItemMeta(lvl2M);
	}
	@SuppressWarnings("deprecation")
	ItemStack lvl3 = new ItemStack(Material.STAINED_GLASS_PANE, 3, DyeColor.LIME.getData());
	{	
		ItemMeta lvl3M = back.getItemMeta();
		lvl3M.setDisplayName(green+"Level 3");
		lvl3.setItemMeta(lvl3M);
	}
	@SuppressWarnings("deprecation")
	ItemStack lvl4 = new ItemStack(Material.STAINED_GLASS_PANE, 4, DyeColor.LIME.getData());
	{	
		ItemMeta lvl4M = back.getItemMeta();
		lvl4M.setDisplayName(green+"Level 4");
		lvl4.setItemMeta(lvl4M);
	}
	@SuppressWarnings("deprecation")
	ItemStack lvl5 = new ItemStack(Material.STAINED_GLASS_PANE, 5, DyeColor.LIME.getData());
	{	
		ItemMeta lvl5M = back.getItemMeta();
		lvl5M.setDisplayName(green+"Level 5");
		lvl5.setItemMeta(lvl5M);
	}	
	
	//Create the enchantment metas
	ItemStack unbreaking = new ItemStack(Material.ENCHANTED_BOOK);
	{
    ItemMeta unbreakingM = unbreaking.getItemMeta();
    unbreakingM.setDisplayName(gold+"Unbreaking");
    unbreaking.setItemMeta(unbreakingM);
	}
    ItemStack mending = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta mendingM = mending.getItemMeta();
    mendingM.setDisplayName(gold+"Mending");
    mending.setItemMeta(mendingM);
    }
    ItemStack sharp = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta sharpM = sharp.getItemMeta();
    sharpM.setDisplayName(gold+"Sharpness");
    sharp.setItemMeta(sharpM);
	}
    ItemStack smite = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta smiteM = smite.getItemMeta();
    smiteM.setDisplayName(gold+"Smite");
    smite.setItemMeta(smiteM);
	}
    ItemStack bane = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta baneM = bane.getItemMeta();
    baneM.setDisplayName(gold+"Bane of Arthropods");
    bane.setItemMeta(baneM);
	}
    ItemStack knockback = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta knockbackM = knockback.getItemMeta();
    knockbackM.setDisplayName(gold+"Knockback");
    knockback.setItemMeta(knockbackM);
    }
    ItemStack faspect = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta faspectM = faspect.getItemMeta();
    faspectM.setDisplayName(gold+"Fire Aspect");
    faspect.setItemMeta(faspectM);
    }
    ItemStack looting = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta lootingM = looting.getItemMeta();
    lootingM.setDisplayName(gold+"Looting");
    looting.setItemMeta(lootingM);
    }
    ItemStack power = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta powerM = power.getItemMeta();
    powerM.setDisplayName(gold+"Power");
    power.setItemMeta(powerM);
	}
    ItemStack punch = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta punchM = punch.getItemMeta();
    punchM.setDisplayName(gold+"Punch");
    punch.setItemMeta(punchM);
    }
    ItemStack flame = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta flameM = flame.getItemMeta();
    flameM.setDisplayName(gold+"Flame");
    flame.setItemMeta(flameM);
    }
    ItemStack infinity = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta infinityM = infinity.getItemMeta();
    infinityM.setDisplayName(gold+"Infinity");
    infinity.setItemMeta(infinityM);
    }
    ItemStack prot = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta protM = prot.getItemMeta();
    protM.setDisplayName(gold+"Protection");
    prot.setItemMeta(protM);
    }
    ItemStack fprot = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta fprotM = fprot.getItemMeta();
    fprotM.setDisplayName(gold+"Fire Protection");
    fprot.setItemMeta(fprotM);
    }
    ItemStack bprot = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta bprotM = bprot.getItemMeta();
    bprotM.setDisplayName(gold+"Blast Protection");
    bprot.setItemMeta(bprotM);
	}
    ItemStack pprot = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta pprotM = pprot.getItemMeta();
    pprotM.setDisplayName(gold+"Projectile Protection");
    pprot.setItemMeta(pprotM);
	}
    ItemStack thorns = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta thornsM = thorns.getItemMeta();
    thornsM.setDisplayName(gold+"Thorns");
    thorns.setItemMeta(thornsM);
	}
    ItemStack resp = new ItemStack(Material.DIAMOND_HELMET);
    {
    ItemMeta respM = resp.getItemMeta();
    respM.setDisplayName(gold+"Respiration");
    resp.setItemMeta(respM);
	}
    ItemStack aa = new ItemStack(Material.DIAMOND_HELMET);
    {
    ItemMeta aaM = aa.getItemMeta();
    aaM.setDisplayName(gold+"Aqua Affinity");
    aa.setItemMeta(aaM);
    }
    ItemStack ffalling = new ItemStack(Material.DIAMOND_BOOTS);
    {
    ItemMeta ffallingM = ffalling.getItemMeta();
    ffallingM.setDisplayName(gold+"Feather Falling");
    ffalling.setItemMeta(ffallingM);
    }
    ItemStack dstrider = new ItemStack(Material.DIAMOND_BOOTS);
    {
    ItemMeta dstriderM = dstrider.getItemMeta();
    dstriderM.setDisplayName(gold+"Depth Strider");
    dstrider.setItemMeta(dstriderM);
    }
    ItemStack fwalker = new ItemStack(Material.DIAMOND_BOOTS);
    {
    ItemMeta fwalkerM = fwalker.getItemMeta();
    fwalkerM.setDisplayName(gold+"Frost Walker");
    fwalker.setItemMeta(fwalkerM);
    }
    ItemStack eff = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta effM = eff.getItemMeta();
    effM.setDisplayName(gold+"Efficiency");
    eff.setItemMeta(effM);
    }
    ItemStack silktouch = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta silktouchM = silktouch.getItemMeta();
    silktouchM.setDisplayName(gold+"Silk Touch");
    silktouch.setItemMeta(silktouchM);
    }
    ItemStack fortune = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta fortuneM = fortune.getItemMeta();
    fortuneM.setDisplayName(gold+"Fortune");
    fortune.setItemMeta(fortuneM);
    }
    ItemStack luckofthesea = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta luckoftheseaM = luckofthesea.getItemMeta();
    luckoftheseaM.setDisplayName(gold+"Luck of the Sea");
    luckofthesea.setItemMeta(luckoftheseaM);
    }
    ItemStack lure = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta lureM = lure.getItemMeta();
    lureM.setDisplayName(gold+"Lure");
    lure.setItemMeta(lureM);
    }
    
    //Attribute icons
    ItemStack maxHP = new ItemStack(Material.GOLDEN_APPLE);
    {
    ItemMeta maxHPM = maxHP.getItemMeta();
    maxHPM.setDisplayName(gold+"Max Health");
    maxHP.setItemMeta(maxHPM);
	}
    ItemStack follow = new ItemStack(Material.LEASH);
    {
    ItemMeta followM = follow.getItemMeta();
    followM.setDisplayName(gold+" Range");
    follow.setItemMeta(followM);
	}
    ItemStack knockbackRes = new ItemStack(Material.SHIELD);
    {
    ItemMeta knockbackResM = knockbackRes.getItemMeta();
    knockbackResM.setDisplayName(gold+"Knockback Resistance");
    knockbackRes.setItemMeta(knockbackResM);
	}
    ItemStack moveSpeed = new ItemStack(Material.SUGAR);
    {
    ItemMeta moveSpeedM = moveSpeed.getItemMeta();
    moveSpeedM.setDisplayName(gold+"Movement Speed");
    moveSpeed.setItemMeta(moveSpeedM);
	}
    ItemStack atkDMG = new ItemStack(Material.DIAMOND_SWORD);
    {
    ItemMeta atkDMGM = atkDMG.getItemMeta();
    atkDMGM.setDisplayName(gold+"Attack Damage");
    atkDMG.setItemMeta(atkDMGM);
    }
    ItemStack armorA = new ItemStack(Material.DIAMOND_CHESTPLATE);
    {
    ItemMeta armorM = armorA.getItemMeta();
    armorM.setDisplayName(gold+" Armor Points");
    armorA.setItemMeta(armorM);
	}
    ItemStack armorToughness = new ItemStack(Material.OBSIDIAN);
    {
    ItemMeta armorToughnessM = armorToughness.getItemMeta();
    armorToughnessM.setDisplayName(gold+"Armor Toughness");
    armorToughness.setItemMeta(armorToughnessM);
    }
    ItemStack atkSpeed = new ItemStack(Material.IRON_SWORD);
    {
    ItemMeta atkSpeedM = atkSpeed.getItemMeta();
    atkSpeedM.setDisplayName(gold+"Attack Speed");
    atkSpeed.setItemMeta(atkSpeedM);
	}
    ItemStack luck = new ItemStack(Material.ENCHANTED_BOOK);
    {
    ItemMeta luckM = luck.getItemMeta();
    luckM.setDisplayName(gold+"Attack Damage");
    luck.setItemMeta(luckM);
    }
    
    //Misc Attributes
    ItemStack unbreakable = new ItemStack(Material.BEDROCK);
    {
    ItemMeta unbreakableM = unbreakable.getItemMeta();
    unbreakableM.setDisplayName(gold+"Unbreakable");
    unbreakable.setItemMeta(unbreakableM);
	}
    ItemStack wipeEnch = new ItemStack(Material.BOOK);
    {
    ItemMeta wipeEnchM = wipeEnch.getItemMeta();
    wipeEnchM.setDisplayName(gold+"Wipe all Enchantments");
    wipeEnch.setItemMeta(wipeEnchM);
    }
    ItemStack wipeData = new ItemStack(Material.DIRT);
    {
    ItemMeta wipeDataM = wipeData.getItemMeta();
    wipeDataM.setDisplayName(gold+"Wipe the item's data");
    wipeData.setItemMeta(wipeDataM);
    }
    
    
    
	//Events
	@SuppressWarnings("deprecation")
	@EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
	    Player p = (Player) e.getWhoClicked();
	    ItemStack clicked = e.getCurrentItem();
	    Inventory inventory = e.getInventory();
	    int slot = e.getRawSlot();
	    
	    //Main Menu
	    if (inventory.getName().equals(mainmenu.getName())) {
	    	//Make an item
	    	if (clicked.getType().equals(Material.APPLE) && p.hasPermission("customitem.item")) {
	    		e.setCancelled(true);
	    		p.openInventory(edititem);
		    }
	    	if (clicked.getType().equals(Material.APPLE) && !p.hasPermission("customitem.item"))
	    	{
	    		e.setCancelled(true);
	    		p.sendMessage(noperms);
	    	}
	    	//Make a firework
	    	if (clicked.getType() == Material.FIREWORK) {
	    		e.setCancelled(true);
	    		p.closeInventory();
	    		p.sendMessage("This feature is currently in development");
//    			p.openInventory(fireworkGUI);
		    }
	    	//Make a spawner
	    	if (clicked.getType() == Material.MOB_SPAWNER) {
	    		e.setCancelled(true);
	    		p.closeInventory();
	    		p.sendMessage("This feature is currently in development");
		    }
	    	//Help
	    	if (clicked.getType() == Material.BOOK) {
	    		e.setCancelled(true);
		    }
	    	if (clicked.getType() == Material.STAINED_GLASS_PANE) {
	    		e.setCancelled(true);
		    }	    	
	    }
	    
	    
	    
	    //Make item
	    if (inventory.getName().equals(edititem.getName())) {
	    	//Rename
	    	if (clicked.getType() == Material.NAME_TAG && p.hasPermission("customitem.item.names")) {
	    		p.openInventory(renameGUI);
	    		e.setCancelled(true);
		    }
	    	if (clicked.getType() == Material.NAME_TAG && !p.hasPermission("customitem.item.names"))
	    	{
	    		e.setCancelled(true);
	    		p.sendMessage(noperms);
	    	}
	    	//Lore
	    	if (clicked.getType() == Material.BOOK && p.hasPermission("customitem.item.lores")) {
	    		p.openInventory(loreGUI);
	    		e.setCancelled(true);
	    	}
	    	if (clicked.getType() == Material.BOOK && !p.hasPermission("customitem.item.lores"))
	    	{
	    		e.setCancelled(true);
	    		p.sendMessage(noperms);
	    	}
	    	//Ench
	    	if (clicked.getType() == Material.ENCHANTED_BOOK && p.hasPermission("customitem.item.enchantments")) {
	    		p.openInventory(enchGUI);
	    		e.setCancelled(true);
		    }
	    	if (clicked.getType() == Material.ENCHANTED_BOOK && !p.hasPermission("customitem.item.enchantments"))
	    	{
	    		e.setCancelled(true);
	    		p.sendMessage(noperms);
	    	}
	    	//Attrib
	    	if (clicked.getType() == Material.DIAMOND_CHESTPLATE) {
	    		e.setCancelled(true);
		    }
	    	//Custom
	    	if (clicked.getType() == Material.TNT && p.hasPermission("customitem.item.misc")) {
	    		p.openInventory(customGUI);
	    		e.setCancelled(true);
		    }
	    	if (clicked.getType() == Material.TNT && !p.hasPermission("customitem.item.misc"))
	    	{
	    		e.setCancelled(true);
	    		p.sendMessage(noperms);
	    	}
	    	//Rest
	    	if (clicked.getType() == Material.STAINED_GLASS_PANE) {
	    		e.setCancelled(true);
		    }
	    	if (clicked.getType() == Material.WOOL) {
	    		e.setCancelled(true);
	    		p.openInventory(mainmenu);
		    }
	    }

	    //Rename
	    if (inventory.getName().equals(renameGUI.getName())) {
	    	//Rename
	    	if (clicked.getType() == Material.EMERALD_BLOCK) {
	    		e.setCancelled(true);
	    		p.closeInventory();
	    		IsEditing.put(p, true);
	    		mode.put(p, "rename");
	    		lore.clear();
		    }
	    	//Remove name
	    	if (clicked.getType() == Material.REDSTONE_BLOCK) {
	    		e.setCancelled(true);
	    		ItemStack inHand = p.getItemInHand();
	    		ItemMeta inHandM = inHand.getItemMeta();
	    		inHandM.setDisplayName("");
	    		inHand.setItemMeta(inHandM);
	    	}
	    	//Rest
	    	if (clicked.getType() == Material.STAINED_GLASS_PANE) {
	    		e.setCancelled(true);
		    }
	    	if (clicked.getType() == Material.WOOL) {
	    		e.setCancelled(true);
	    		p.openInventory(edititem);
		    }
	    }
	    
	    
	    //Lore
	    if (inventory.getName().equals(loreGUI.getName())) {
	    	//Relore
	    	if (clicked.getType() == Material.EMERALD_BLOCK) {
	    		e.setCancelled(true);
	    		p.closeInventory();
	    		IsEditing.put(p, true);
	    		mode.put(p, "lore");
	    		p.sendMessage(ChatColor.GREEN+"Type \"cancel\" or hit enter to stop editing");
		    }
	    	//Remove lore
	    	if (clicked.getType() == Material.REDSTONE_BLOCK) {
	    		e.setCancelled(true);
	    		ItemStack inHand = p.getItemInHand();
	    		ItemMeta inHandM = inHand.getItemMeta();
	    		lore.clear();
	    		inHandM.setLore(lore);
	    		inHand.setItemMeta(inHandM);
	    	}
	    	//Rest
	    	if (clicked.getType() == Material.STAINED_GLASS_PANE) {
	    		e.setCancelled(true);
		    }
	    	if (clicked.getType() == Material.WOOL) {
	    		e.setCancelled(true);
	    		p.openInventory(edititem);
		    }
	    }
	    
	    //Enchant
	    if (inventory.getName().equals(enchGUI.getName())) {
	    	//Global
	    	if (clicked.getItemMeta().getDisplayName().equals(global.getItemMeta().getDisplayName())) {
	    		e.setCancelled(true);
	    		enchLvl.put(p, 1);
	    		p.openInventory(enchGUIglobal);
		    }
	    	//Sword
	    	if (clicked.getItemMeta().getDisplayName().equals(sword.getItemMeta().getDisplayName())) {
	    		e.setCancelled(true);
	    		enchLvl.put(p, 1);
	    		p.openInventory(enchGUIsword);
		    }
	    	//Bow
	    	if (clicked.getType().equals(Material.BOW)) {
	    		e.setCancelled(true);
	    		enchLvl.put(p, 1);
	    		p.openInventory(enchGUIbow);
		    }
	    	//Armor
	    	if (clicked.getType() == Material.DIAMOND_CHESTPLATE) {
	    		e.setCancelled(true);
	    		enchLvl.put(p, 1);
	    		p.openInventory(enchGUIarmor);
		    }
	    	//Tools
	    	if (clicked.getType().equals(Material.DIAMOND_PICKAXE)) {
	    		e.setCancelled(true);
	    		enchLvl.put(p, 1);
	    		p.openInventory(enchGUItools);
		    }
	    	//Rest
	    	if (clicked.getType() == Material.WOOL) {
	    		e.setCancelled(true);
	    		p.openInventory(edititem);
		    }
	    }
	    
	    if (inventory.getName().equals(enchGUIglobal.getName()) || inventory.getName().equals(enchGUIsword.getName()) || inventory.getName().equals(enchGUIbow.getName()) || inventory.getName().equals(enchGUIarmor.getName()) || inventory.getName().equals(enchGUItools.getName())) {
	    	//Levels
	    	if (clicked.getItemMeta().getDisplayName().equals(rem.getItemMeta().getDisplayName())) {
	    		e.setCancelled(true);
	    		enchLvl.put(p, 0);
		    }
	    	if (clicked.getItemMeta().getDisplayName().equals(lvl1.getItemMeta().getDisplayName())) {
	    		e.setCancelled(true);
	    		enchLvl.put(p, 1);
		    }
	    	if (clicked.getItemMeta().getDisplayName().equals(lvl2.getItemMeta().getDisplayName())) {
	    		e.setCancelled(true);
	    		enchLvl.put(p, 2);
		    }
	    	if (clicked.getItemMeta().getDisplayName().equals(lvl3.getItemMeta().getDisplayName())) {
	    		e.setCancelled(true);
	    		enchLvl.put(p, 3);
		    }
	    	if (clicked.getItemMeta().getDisplayName().equals(lvl4.getItemMeta().getDisplayName())) {
	    		e.setCancelled(true);
	    		enchLvl.put(p, 4);
		    }
	    	if (clicked.getItemMeta().getDisplayName().equals(lvl5.getItemMeta().getDisplayName())) {
	    		e.setCancelled(true);
	    		enchLvl.put(p, 5);
		    }
	    	
	    	if (clicked.getType().equals(Material.ENCHANTED_BOOK) || clicked.getType().equals(Material.FISHING_ROD) || clicked.getType().equals(Material.DIAMOND_HELMET) || clicked.getType().equals(Material.DIAMOND_BOOTS))
	    	{
	    		if(!enchLvl.get(p).equals(null)&& !enchLvl.get(p).equals(0))
	    		{
	    			e.setCancelled(true);
	    		
		    		//Global
		    		if (clicked.getItemMeta().getDisplayName().equals(unbreaking.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.DURABILITY, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(mending.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.MENDING, enchLvl.get(p));
		    		}
		    		
		    		//Sword
		    		if (clicked.getItemMeta().getDisplayName().equals(sharp.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(smite.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.DAMAGE_UNDEAD, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(bane.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.DAMAGE_ARTHROPODS, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(knockback.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.KNOCKBACK, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(faspect.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(looting.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.LOOT_BONUS_MOBS, enchLvl.get(p));
		    		}
		    		
		    		//Bow
		    		if (clicked.getItemMeta().getDisplayName().equals(power.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(punch.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.ARROW_KNOCKBACK, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(flame.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.ARROW_FIRE, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(infinity.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, enchLvl.get(p));
		    		}
		    		
		    		//Armor
		    		if (clicked.getItemMeta().getDisplayName().equals(prot.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(fprot.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.PROTECTION_FIRE, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(bprot.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.PROTECTION_EXPLOSIONS, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(pprot.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.PROTECTION_PROJECTILE, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(thorns.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.THORNS, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(resp.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.OXYGEN, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(aa.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.WATER_WORKER, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(ffalling.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(dstrider.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.DEPTH_STRIDER, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(fwalker.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.FROST_WALKER, enchLvl.get(p));
		    		}
		    		
		    		//Tools
		    		if (clicked.getItemMeta().getDisplayName().equals(eff.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.DIG_SPEED, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(silktouch.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.SILK_TOUCH, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(fortune.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.LOOT_BONUS_BLOCKS, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(luckofthesea.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.LUCK, enchLvl.get(p));
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(lure.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.addUnsafeEnchantment(Enchantment.LURE, enchLvl.get(p));
		    		}
	    		}
	    		
	    		
	    		//Unenchanting
	    		if(enchLvl.get(p).equals(null) || enchLvl.get(p).equals(0))
	    		{
	    			e.setCancelled(true);
	    		
		    		//Global
		    		if (clicked.getItemMeta().getDisplayName().equals(unbreaking.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.DURABILITY);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(mending.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.MENDING);
		    		}
		    		
		    		//Sword
		    		if (clicked.getItemMeta().getDisplayName().equals(sharp.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.DAMAGE_ALL);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(smite.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.DAMAGE_UNDEAD);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(bane.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.DAMAGE_ARTHROPODS);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(knockback.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.KNOCKBACK);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(faspect.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.FIRE_ASPECT);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(looting.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.LOOT_BONUS_MOBS);
		    		}
		    		
		    		//Bow
		    		if (clicked.getItemMeta().getDisplayName().equals(power.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.ARROW_DAMAGE);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(punch.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.ARROW_KNOCKBACK);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(flame.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.ARROW_FIRE);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(infinity.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.ARROW_INFINITE);
		    		}
		    		
		    		//Armor
		    		if (clicked.getItemMeta().getDisplayName().equals(prot.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(fprot.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.PROTECTION_FIRE);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(bprot.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.PROTECTION_EXPLOSIONS);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(pprot.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.PROTECTION_PROJECTILE);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(thorns.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.THORNS);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(resp.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.OXYGEN);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(aa.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.WATER_WORKER);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(ffalling.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.PROTECTION_FALL);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(dstrider.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.DEPTH_STRIDER);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(fwalker.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.FROST_WALKER);
		    		}
		    		
		    		//Tools
		    		if (clicked.getItemMeta().getDisplayName().equals(eff.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.DIG_SPEED);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(silktouch.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.SILK_TOUCH);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(fortune.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.LOOT_BONUS_BLOCKS);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(luckofthesea.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.LUCK);
		    		}
		    		if (clicked.getItemMeta().getDisplayName().equals(lure.getItemMeta().getDisplayName()))
		    		{
		    			ItemStack inHand = p.getItemInHand();
			    		inHand.removeEnchantment(Enchantment.LURE);
		    		}
	    		}	    		
	    		
	    		
	    	}
	    	
	    	
	    	//Rest
	    	if (clicked.getItemMeta().getDisplayName().equals(filler.getItemMeta().getDisplayName())) {
	    		e.setCancelled(true);
		    }
	    	if (clicked.getType() == Material.WOOL) {
	    		e.setCancelled(true);
	    		p.openInventory(enchGUI);
		    }
	    }
	     
	    
	    //Misc Attrib
	    if (inventory.getName().equals(customGUI.getName())) {
	    	//Unbreakable
	    	if (clicked.getItemMeta().getDisplayName().equals(unbreakable.getItemMeta().getDisplayName())) {
	    		e.setCancelled(true);
	    		ItemStack inHand = p.getItemInHand();
	    		ItemMeta inHandM = inHand.getItemMeta();
	    		if (inHandM.spigot().isUnbreakable())
	    		{
	    			inHandM.spigot().setUnbreakable(false);
	    		}
	    		else
	    		{
	    			inHandM.spigot().setUnbreakable(true);
	    		}
	    		inHand.setItemMeta(inHandM);
	    		
		    }
	    	
	    	if (clicked.getItemMeta().getDisplayName().equals(wipeEnch.getItemMeta().getDisplayName())) {
	    		e.setCancelled(true);
	    		ItemStack inHand = p.getItemInHand();
	    		ItemMeta inHandM = inHand.getItemMeta();
	    	    for (Enchantment ench : inHandM.getEnchants().keySet()) {
	    	    	inHandM.removeEnchant(ench);
	    	    }
	    		inHand.setItemMeta(inHandM);
		    }
	    	
	    	if (clicked.getItemMeta().getDisplayName().equals(wipeData.getItemMeta().getDisplayName())) {
	    		e.setCancelled(true);
	    		ItemStack inHand = p.getItemInHand();
	    		ItemStack inHandFresh = new ItemStack (p.getItemInHand().getType());
	    	    ItemMeta inHandFreshM = inHandFresh.getItemMeta();
	    		inHand.setItemMeta(inHandFreshM);
		    }
	    	
	    	//Rest
	    	if (clicked.getType() == Material.STAINED_GLASS_PANE) {
	    		e.setCancelled(true);
		    }
	    	if (clicked.getType() == Material.WOOL) {
	    		e.setCancelled(true);
	    		p.openInventory(edititem);
		    }
	    }
	    
	    
    }
	
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent ev){
		Player pl = ev.getPlayer();
		if (IsEditing.get(pl) == true){
			ev.setCancelled(true);
			input = ev.getMessage();
			colorInput = ChatColor.translateAlternateColorCodes('&', input); 
			IsEditing.put(pl, false);
			
			//Rename
			if (mode.get(pl) == "rename")
			{
				@SuppressWarnings("deprecation")
				ItemStack inHand = pl.getItemInHand();
	    		ItemMeta inHandM = inHand.getItemMeta();
	    		inHandM.setDisplayName(ChatColor.WHITE+colorInput);
	    		inHand.setItemMeta(inHandM);
	    		mode.put(pl, "");
	    		pl.sendMessage(ChatColor.GREEN+"Successfly renamed item to: "+ChatColor.WHITE+colorInput);
			}
			
			//Lore
			if (mode.get(pl) == "lore")
			{
				@SuppressWarnings("deprecation")
				ItemStack inHand = pl.getItemInHand();
	    		ItemMeta inHandM = inHand.getItemMeta();
	    		
	    		if (input.equalsIgnoreCase("cancel") || input.equals(""))
				{
    			IsEditing.put(pl, false);
	    		inHandM.setLore(lore);
	    		inHand.setItemMeta(inHandM);
	    		mode.put(pl, "");
	    		pl.sendMessage(ChatColor.GREEN+"Successfly set the item's lore");
				}
	    		else
	    		{
	    			lore.add(ChatColor.DARK_PURPLE+""+ChatColor.ITALIC+colorInput);
	    			IsEditing.put(pl, true);
	    			pl.sendMessage(ChatColor.GREEN+"Added lore line: "+ChatColor.DARK_PURPLE+""+ChatColor.ITALIC+colorInput);
	    		}
			}
			
//			if (mode.get(pl) == "lore")
//			{
//				mode.put(pl, "loreStep2");
//				loreLine.put(pl, Integer.parseInt(input));
//				IsEditing.put(pl, true);
//			}
		}
		
	}
	
}
